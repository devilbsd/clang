/* Registro e impresion de usuarios
 * La coleccion de registros es almacenada en
 * una lista enlazada elastica. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>
#include <ctype.h>
#include <time.h>>

void clrscr()
{
  system("@cls||clear");
}

int main (void)
{


/*Definicion de un nodo en la lista*/
typedef struct nodo{
  char nombre[25], apellido[25], genero[20];
  float peso, estatura;
  int diaNacimiento, mesNacimiento, anioNacimiento; 
  struct nodo *siguiente; /*define de forma recursiva el apuntador al tipo nodo*/
}nodo_type;

 int opcionMenu=1, opcion, iter, eofdb = 0, operations;
float imc, peso, estatura;
time_t fechaActual;


/* INICIALIZA EL STORAGE DE LOS REGISTROS
 * primer apuntador a un tipo nodo_type inicializado a NULL, no apunta a ningun lado
 * head == NULL representa una lista con cero nodos */
struct nodo *head = NULL;
struct nodo *nuevoNodo = NULL;
struct nodo *blob = NULL;

/* Antes de mostrar el menu principal el programa intenta
 * cargar en memoria el archivo registros.csv que debe
 * estar en el mismo directorio.
 * Cargar en memoria significa construir la lista enlazada.
 */

/* variables para obtener los datos del csv */
 

 FILE *dbFile;
 char nombre[25], apellido[25],genero[20];
 int diaNac, mesNac, anioNac;
 //float estatura, peso;

 /*variables para blob */
 int intTemp;
 char charTemp[25];
 float floatTemp;

 
 dbFile = fopen("registros.db", "a+");

 /* nuevoNodo = malloc(sizeof(struct nodo));*/
  

 if (dbFile == NULL) {
   printf("No existen registros previos..\n");
   /*exit(1);*/
 }

 
 /* Popular una lista enlazada con los datos 
  * obtenidos del archivo, fscanf ha funcionado
  * bien para leer los datos, pero ha dado problemas
  * para leerlo en formato csv por eso se ha 
  * optado por qu el formato sea separado por
  * espacios.
  */

 while ( eofdb == 0)
   {
     
     nuevoNodo = malloc(sizeof(struct nodo));
     
     if (fscanf(dbFile, "%s %s %s %d %d %d %f %f",
	  &nuevoNodo->nombre, &nuevoNodo->apellido, &nuevoNodo->genero,
	  &nuevoNodo->diaNacimiento, &nuevoNodo->mesNacimiento,
	  &nuevoNodo->anioNacimiento,&nuevoNodo->estatura,
		   &nuevoNodo->peso ) != EOF){
   
       //printf("nN despues while: %d\n",nuevoNodo);		
       //      printf("%s ",nuevoNodo->nombre);
  
       //nuevoNodo->peso = peso;
       //   printf("%s %.2f\n", nombre, peso);
       //printf("head: %d\n",head);
       
       if (head == NULL){
       /*printf("La lista esta vacia");*/
       nuevoNodo->siguiente=0;
       head = nuevoNodo;
     }
       else{
       /*printf("La lista ya tiene elementos, hay que ajustar");*/
       nuevoNodo->siguiente=head;
       head = nuevoNodo;
     }
     }
       else{ eofdb =1;}
 }

 
 
/* Menu principal */
while (opcionMenu !=0)
{
  printf("\n-| REGISTRO DE USUARIOS |-\n");
  printf("===========================\n\n");
  printf("1. Agregar registro.\n2. Mostrar registros.\n");
  printf("3. Editar registro.\n4. Eliminar registro.\n");
  printf("5. Ordenar registros.\n6. Acerca de.\n0. Salir\n\n");

  printf("=> ");
  scanf ("%d",&opcionMenu);
  //clrscr(); 
  switch (opcionMenu){


  case 1: /* AGREGA NUEVOS REGISTROS*/
    opcion=1;
    while (opcion !=0){
	/* malloc entrega direccion de espacio alojado en heap*/
    nuevoNodo = malloc(sizeof(struct nodo));
    /*
    printf("\nDireccion de head %d\n",&head);
    printf("Valor de head %d\n",head);
    printf("Valor del puntero NuevoNodo: referencia la nuevo espacio %d\n",nuevoNodo);
    printf("Direccion de nuevoNodo %d\n",&nuevoNodo);
    */
    printf("\n-| Tarjeta de Registro |-");
    printf("\n=========================\n\n");
    printf("Nombre: ");
    scanf ("%s",&nuevoNodo->nombre);
    printf("\nApellido: ");
    scanf ("%s",&nuevoNodo->apellido);
    printf("\nGenero (f/m): ");
    scanf ("%s",&nuevoNodo->genero);
    printf("\nDia de Nacimiento ");
    scanf ("%d",&nuevoNodo->diaNacimiento);
    printf("\nMes de Nacimiento: 1=Enero 2=Febrero ");
    scanf ("%d",&nuevoNodo->mesNacimiento);
    printf("\nAnio de Nacimiento: ");
    scanf ("%d",&nuevoNodo->anioNacimiento);
    printf("\nEstatura en metro (1.70): ");
    scanf ("%f",&nuevoNodo->estatura);
    printf("\nPeso en kg: ");
    scanf ("%f",&nuevoNodo->peso);
    printf ("\n");
    printf("=> Capturar otro usuario? [0=SALIR, 1=Continuar]");
    scanf ("%d",&opcion);

    /* Agregar usuario al archivo */
    fprintf(dbFile,"%s %s %s %d %d %d %f %f\n",
	 nuevoNodo->nombre, nuevoNodo->apellido, nuevoNodo->genero,
	 nuevoNodo->diaNacimiento, nuevoNodo->mesNacimiento,
	 nuevoNodo->anioNacimiento, nuevoNodo->estatura,
	 nuevoNodo->peso);
 
    if (head == NULL){
      printf("La lista esta vacia");
      nuevoNodo->siguiente=NULL;
      head = nuevoNodo;
    }
    else{
      printf("La lista ya tiene elementos, hay que ajustar");
      nuevoNodo->siguiente=head;
      head = nuevoNodo;
    }
    
    printf("\nDireccion de head %d\n",&head);
    printf("Valor de head %d\n",head);
    printf("Valor del puntero NuevoNodo: referencia al nuevo espacio %d\n",nuevoNodo);
    printf("Direccion de nuevoNodo %d\n",&nuevoNodo);
    printf("Donde apunta ahora nuevoNodo %d\n",nuevoNodo->siguiente);
    }
    break;



  case 2: /* IMPRIME TODOS LOS REGISTROS*/
    clrscr();
    printf("\nID %10s %10s %11s %4s %4s Altura Peso IMC\n",
	   "Nombre","Apellido","FechaNac","Edad","Sexo");
    printf("=========================================================================\n");
    /* nuevoNodo va a ser el apuntador para recorrer la lista, copiamos
     * la direccion de comienzo para trabajar con ella */
	
    nuevoNodo = head;
    iter = 0;
    fechaActual=time(NULL);
  
    while (nuevoNodo != NULL){
 
      peso = nuevoNodo->peso;
      estatura = nuevoNodo->estatura;
      imc = nuevoNodo->peso / (nuevoNodo->estatura * nuevoNodo->estatura);
	
      printf("[%2d] %10s %10s %2d/%2d/%4d %4d %3s  %3.2fm %5.1fkg %3.2f \n",
	     iter+1, nuevoNodo->nombre, nuevoNodo->apellido,
	     nuevoNodo->diaNacimiento, nuevoNodo->mesNacimiento,
	     nuevoNodo->anioNacimiento,
	     ((localtime(&fechaActual)->tm_year+1900) - nuevoNodo->anioNacimiento),
	     nuevoNodo->genero, estatura, peso, imc);
      nuevoNodo = nuevoNodo->siguiente;
      iter++;
    }
    
    printf("\nTotal de Registros: %d\n",iter);
    break;
    

  case 3:
    nuevoNodo = head;
    iter = 0;
    intTemp = 1;
    
    printf("\n-| REGISTRO > EDICION |-\n");
    printf("==========================\n\n");
    printf("ID del registro a modificar:\n");
    printf("=> ");
    scanf ("%d",&opcion);
    
    
    while (nuevoNodo !=NULL){
      if (opcion == iter+1){
	//clrscr();
	while (intTemp != 0){
	  clrscr();
	printf("\n-| Datos del Registro |-");
	printf("\n=========================\n\n");
	printf("1) Nombre: %s",&nuevoNodo->nombre);
	printf("\n2) Apellido:%s",&nuevoNodo->apellido);
	printf("\n3) Genero (f/m): %s",&nuevoNodo->genero);
	printf("\n4) Dia de Nacimiento: %d",nuevoNodo->diaNacimiento);
	printf("\n5) Mes de Nacimiento: %d",nuevoNodo->mesNacimiento);
	printf("\n6) Anio de Nacimiento: %d",nuevoNodo->anioNacimiento);
	printf("\n7) Estatura: %.2f m",nuevoNodo->estatura);
	printf("\n8) Peso: %.2f kg",nuevoNodo->peso);
	printf("\n\n0) Salir.");
	printf ("\n\n");
	printf("Que dato desea modificar?\n=> ");
	scanf ("%d",&intTemp);

	switch(intTemp){

	case 1:
	  printf ("\n");
	  printf("Ingrese nuevo nombre:\n=> ");
	  scanf ("%s",&nuevoNodo->nombre);
	  break;

	case 2:
	  printf ("\n");
	  printf("Ingrese nuevo apellido:\n=> ");
	  scanf ("%s",&nuevoNodo->apellido);
	  break;

	case 3:
	  printf ("\n");
	  printf("Ingrese genero M/F:\n=> ");
	  scanf ("%s",&nuevoNodo->genero);
	  break;

	case 4:
	  printf ("\n");
	  printf("Ingrese dia de nacimiento:\n=> ");
	  scanf ("%d",&nuevoNodo->diaNacimiento);
	  break;

	case 5:
	  printf ("\n");
	  printf("Ingrese mes de nacimiento:\n=> ");
	  scanf ("%d",&nuevoNodo->mesNacimiento);
	  break;

	case 6:
	  printf ("\n");
	  printf("Ingrese anio de nacimiento:\n=> ");
	  scanf ("%d",&nuevoNodo->anioNacimiento);
	  break;

	case 7:
	  printf ("\n");
	  printf("Ingrese nueva estatura:\n=> ");
	  scanf ("%f",&nuevoNodo->estatura);
	  break;

	case 8:
	  printf ("\n");
	  printf("Ingrese nuevo peso:\n=> ");
	  scanf ("%f",&nuevoNodo->peso);
	  break;



	case 0:
	  break;
	}//Switch
	}
      }//if
      else{;}

      nuevoNodo = nuevoNodo->siguiente;
      iter++;
    } //while
    break;

  case 4:
    
    nuevoNodo = head;
    iter = 0;
    printf("\n-| REGISTRO > ELIMINAR |-\n");
    printf("==========================\n\n");
    printf("ID del registro a ELIMINAR:\n");
    printf("=> ");
    scanf ("%d",&opcion);
    
    while (nuevoNodo !=NULL){

      if (opcion == iter+1){
       
	printf("Deseas eliminar registro: %s %s? 1=SI 0=NO\n=> ",
	       nuevoNodo->nombre, nuevoNodo->apellido);
	scanf ("%d",&intTemp);

	switch(intTemp){

	case 1:
	  if (head == nuevoNodo){
	    head = nuevoNodo->siguiente;
	    free(nuevoNodo);
	  }
	  else{
	    memcpy(nuevoNodo, nuevoNodo->siguiente, sizeof(struct nodo));
	  }
	
	  
	case 0:
	  break;
	}
      }
      else{;}

      nuevoNodo = nuevoNodo->siguiente;
      iter++;
    }
    break;

  case 5:
      printf("\n-|REGISTRO > ORDENAR |-\n"
	     "========================\n\n");
      printf("1. Ascendente por Edad\n2. Descendente por Edad\n\n");
      printf("=> ");
      scanf ("%d",&opcion);
      
      switch (opcion){
	
      case 1:
	/* Ordenar la lista con los valores mayores
	 * al inicio de la lista: 10,9,8,7,6,5,4,3,2,1,0
	 * Mientras existan cambios: operaciones > 1 seguir
	 * haciendo cambios en la lista
	 */
	operations = 1;
	
	while ( operations != 0 ){
	  iter = 0; //contador de operacioes
	  nuevoNodo = head;
	
	  while (nuevoNodo->siguiente != NULL){
	    if (nuevoNodo->anioNacimiento > nuevoNodo->siguiente->anioNacimiento){
	      iter++;
	      printf("#");
	      /* LA BURBUJA =
	       * El valor del primer nodo se introduce en una estructura temporal
	       * la estructura se almacena mediante malloc y es liberada al final
	       * de la rutina.
	       * 1. BLOB = N1
	       * 2. N1 = N2
	       * 3. N2 = BLOB
	       */
	      blob = malloc(sizeof(struct nodo));
	      /* BLOB = N1    Copia la estructura completa,
	       * N1->siguiente apunta a N2, ya que estara en el lugar de N2,
	       * debe apunta a N3, 
	       * N1->sigueinte->siguiente
	       */
	      memcpy(blob, nuevoNodo, sizeof(struct nodo));
	      blob->siguiente = nuevoNodo->siguiente->siguiente;
	      //N1 = N2
 	      intTemp = nuevoNodo->siguiente;
	      memcpy(nuevoNodo, nuevoNodo->siguiente, sizeof(struct nodo));
	      nuevoNodo->siguiente = intTemp;
	      //N2 = BLOB
	      memcpy(nuevoNodo->siguiente, blob, sizeof(struct nodo));
	    } 
	    else{;}
	    nuevoNodo = nuevoNodo->siguiente;
	    operations = iter;
	    
	  }
	printf(" Swaps: %d\n ",operations);
	}
	  break;


      case 2:
	/* Ordenar la lista con los valores mayores
	 * al inicio de la lista: 10,9,8,7,6,5,4,3,2,1,0
	 * Mientras existan cambios: operaciones > 1 seguir
	 * haciendo cambios en la lista
	 */
	operations = 1;
	
	while ( operations != 0 ){
	  iter = 0; //contador de operacioes
	  nuevoNodo = head;
		
	  while (nuevoNodo->siguiente != NULL){
	    if (nuevoNodo->anioNacimiento < nuevoNodo->siguiente->anioNacimiento){
	      iter++;
	      /* LA BURBUJA =
	       * El valor del primer nodo se introduce en una estructura temporal
	       * la estructura se almacena mediante malloc y es liberada al final
	       * de la rutina.
	       * 1. BLOB = N1
	       * 2. N1 = N2
	       * 3. N2 = BLOB
	       */
	      blob = malloc(sizeof(struct nodo));
	      /* BLOB = N1    Copia la estructura completa,
	       * N1->siguiente apunta a N2, ya que estara en el lugar de N2,
	       * debe apunta a N3, 
	       * N1->sigueinte->siguiente
	       */
	      memcpy(blob, nuevoNodo, sizeof(struct nodo));
	      blob->siguiente = nuevoNodo->siguiente->siguiente;
	      //N1 = N2
 	      intTemp = nuevoNodo->siguiente;
	      memcpy(nuevoNodo, nuevoNodo->siguiente, sizeof(struct nodo));
	      nuevoNodo->siguiente = intTemp;
	      //N2 = BLOB
	      memcpy(nuevoNodo->siguiente, blob, sizeof(struct nodo));
	    } 
	    else{;}
	    nuevoNodo = nuevoNodo->siguiente;
	    operations = iter;
	    //printf("%d",operations);
	  }}}
	
	break;
      
  case 6:
    printf("\nAlumna: Luz Maria Paredes Ruiz");
    printf("\nMaistro: ");
    break;
    
  case 0:
    /* Antes de terminar el programa, se actualizan todos los
     * registros que estan en el archivos con las modificaciones
     * de la lista en memoria, excluendo los registros marcados
     * como borrados.
     */
    nuevoNodo = head;
    fclose(dbFile);
    dbFile = fopen("registros.db", "w");
    
    while(nuevoNodo != NULL){
      //    printf("%s\n",nuevoNodo->nombre);
      /* Agregar usuarios al archivo*/ 
    fprintf(dbFile,"%s %s %s %d %d %d %.2f %.2f\n",
	 nuevoNodo->nombre, nuevoNodo->apellido, nuevoNodo->genero,
	 nuevoNodo->diaNacimiento, nuevoNodo->mesNacimiento,
	 nuevoNodo->anioNacimiento, nuevoNodo->estatura,
	 nuevoNodo->peso);
    nuevoNodo = nuevoNodo->siguiente;
    }
    fclose(dbFile);
    break;
        
  }
 }
    return 0;
       
}

