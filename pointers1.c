#include <stdio.h>


int main (){


int value;
int *pointer;

typedef struct{
    char nombre[25], apellido[25], genero[25];
    float peso, estatura;
}datosPersonales;

datosPersonales *structpointer;
pointer = &value;


printf("Valor de la variable -value- : %d\n",value);
printf("Direccion donde esta -value- : %d\n",&value);
printf("Direccion donde esta -value- como valor de pointer: %d\n",pointer);
printf("El valor de -value- mostrada por pointer: %d\n",*pointer);
printf("Direccion donde esta -pointer-: %d\n\n",&pointer);

value = 5;

printf("Valor de la variable -value- : %d\n",value);
printf("Direccion donde esta -value- : %d\n",&value);
printf("Direccion donde esta -value- como valor de pointer: %d\n",pointer);
printf("El valor de -value- mostrada por pointer: %d\n",*pointer);
printf("Direccion donde esta -pointer-: %d\n\n",&pointer);

printf("Cantidad de bytes a reservar: %d\n",sizeof(*pointer));

pointer = malloc(sizeof(*pointer));

printf("Nueva Direccion reservada entregada por malloc, valor de pointer, : %d\n",pointer);
printf("El valor de dentro del nuevo espacio reservado: %d\n",*pointer);
printf("Direccion donde esta -pointer-: %d\n\n",&pointer);

*pointer = 666;

printf("Nueva Direccion reservada entregada por malloc, valor de pointer, : %d\n",pointer);
printf("El valor de dentro del nuevo espacio reservado: %d\n",*pointer);
printf("Direccion donde esta -pointer-: %d\n\n",&pointer);


printf("Cantidad de bytes a reservar: %d\n",sizeof(*structpointer));
printf("Cantidad de bytes a reservar: %d\n",sizeof(structpointer));
structpointer = malloc(sizeof(*structpointer));

printf("Nueva Direccion reservada entregada por malloc, valor de structpointer, : %d\n",structpointer);
printf("El valor de dentro del nuevo espacio reservado: %d\n",*structpointer);
printf("Direccion donde esta -structpointer-: %d\n\n",&structpointer);



printf("Nueva Direccion reservada entregada por malloc, valor de structpointer, : %d\n",structpointer);
printf("El valor de dentro del nuevo espacio reservado: %d\n",*structpointer);
printf("Direccion donde esta -structpointer-: %d\n",&structpointer);

}
